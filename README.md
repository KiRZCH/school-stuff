# **School-Stuff Projects on Git**

## The kind of stuff. [^2]

### XML / JSON

 - XML with CSS

 - XML to JSON

 - XML Verification with XSD

### JavaScript

 - Forms

 - Basics

### SQL

 - E/R Diagram, Logical and SQL DBs [^1]

 - SQL Queries

### Java

 - Basic Operations

 - Threads

 - Sockets

### C#

 - Basic Operations

 - Math Operations

 - Threads

### Business Economics

 - Excel Solver

 - Excel Pivot Tables

 - **SOON**
 	Gantt Diagram


## Info

 - GitLab page : [Here](https://kirzch.gitlab.io/school-stuff "Gitlab Page")


## Notes

 [^1]: **Tested on SQLite DBs**

 [^2]: Probably all kind of stuff is in italian

