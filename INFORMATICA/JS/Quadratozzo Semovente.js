function caricato() {
    var body = document.getElementById('body');
    var b = document.getElementById('box');
    var x = 0, y = 0;
    var dx = 5, dy = 6;
    var w = 1920, h = 950;
    var passo = function () {
        b.style.left = x + 'px';
        b.style.top = y + 'px';
        x += dx;
        y += dy;
        if (x > w || x < 0) {
            dx *= -1;
            b.style.backgroundColor = 'red';
            body.style.backgroundColor = 'blue';
            b.style.width = '50px';
            b.style.height = '50px';
        }
        if (y > h || y < 0) {
            dy *= -1;
            b.style.backgroundColor = 'blue';
            body.style.backgroundColor = 'red';
            b.style.width = '30px';
            b.style.height = '30px';
        }
        setTimeout(passo, 10);
    };
    passo();
}