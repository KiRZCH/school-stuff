var bookmark = {
    "sito" : [
        {
            "nome" : "Google",
            "URL" : "https://www.google.com",
            "descrizione" : "Famoso motore di ricerca",
            "categoria" : "Motori di Ricerca"
        },
        {
            "nome" : "La Repubblica",
            "URL" : "https://www.repubblica.it",
            "descrizione" : "Versione online del quotidiano",
            "categoria" : "News"
        },
        {
            "nome" : "Wikipedia",
            "URL" : "https://www.wikipedia.org",
            "descrizione" : "Dispensatore di informazioni",
            "categoria" : "Motori di Ricerca"
        },
        {
            "nome" : "Gian Cristianello Montalcini Albanellato",
            "URL" : "https://www.giancrimonalban.camorra.it",
            "descrizione" : "Killer napoletano su commissione",
            "categoria" : "Divertimento"
        },
        {
            "nome" : "Lo Snobbino di Milano",
            "URL" : "https://www.apealforisalone.cocktail.it",
            "descrizione" : "Milanese che consiglia driks",
            "categoria" : "Noia"
        }
    ],
    "persona" : [
        {
            "nome" : "Giovanni",
            "cognome" : "Giovannini",
            "lavora" : "Google"
        },
        {
            "nome" : "Gianni",
            "cognome" : "Giannini",
            "lavora" : "Wikipedia"
        },
        {
            "nome" : "Giorgio",
            "cognome" : "Giorgini",
            "lavora" : "Lo Snobbino di Milano"
        }
    ]
}
document.write("<h2>Siti e persone: </h2>");
for (i = 0; i < bookmark.sito.length; i++)
{
    document.write("<h3>" + bookmark.sito[i].nome + "</h3>");
    document.write("<ol>");
    document.write("<li> <b>Nome</b>: ");
    document.write(bookmark.sito[i].nome);
    document.write("</li>");
    document.write("<li> <b>URL</b>: ");
    document.write(bookmark.sito[i].URL);
    document.write("</li>");
    document.write("<li> <b>Descrizione</b>: ");
    document.write(bookmark.sito[i].descrizione);
    document.write("</li>");
    document.write("<li> <b>Categoria</b>: ");
    document.write(bookmark.sito[i].categoria);
    document.write("</li>");
    document.write("</ol>");
}
for (x = 0; x < bookmark.persona.length; x++)
{
    document.write("<p> <b>" + bookmark.persona[x].nome + " " + bookmark.persona[x].cognome + "</b>, lavora presso <b>" + bookmark.persona[x].lavora + "</b> </p>");
}