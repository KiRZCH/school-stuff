package Binary;

import java.util.Scanner;

public class BinaryCalc 
{
        public static void main(String[] args) 
        {
            int dec, quot, i = 1, j;
            int bin[] = new int[100];
            Scanner in = new Scanner(System.in);
            System.out.print("Inserisci un numero decimale tra 0 e " + Integer.MAX_VALUE + " : ");
            dec = in.nextInt();
            quot = dec;
            in.close();
            while (quot != 0)
            {
                bin[i++] = quot % 2;
                quot = quot / 2;
            }
            System.out.print("Il numero binario e': ");
            for (j = i; j > 0; j--)
            {
                System.out.print(bin[j]);
            }
            System.out.print("\n");
        }
}
