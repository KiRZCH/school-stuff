package Socket.FirstSocket;

import java.io.*;
import java.net.*;

public class SocketServer 
{
    ServerSocket server;
    Socket client;
    String stringaricevuta;
    String stringamodificata;
    BufferedReader indalclient;
    DataOutputStream outversoclient;

    public Socket attendi()
    {
        try 
        {
            System.out.println("1 SERVER in esecuzione..");
            server = new ServerSocket(6789);
            client = server.accept();
            server.close();
            indalclient = new BufferedReader(new InputStreamReader(client.getInputStream()));
            outversoclient = new DataOutputStream(client.getOutputStream());
        } 
        catch (Exception e) 
        {
            System.out.println(e.getMessage());
            System.out.println("Errore durante la connessione..");
            System.exit(1);
        }
        return client;
    }
    
    public void comunica()
    {
        try 
        {
            System.out.println("3 benvenuto client, scrivi una frase..");
            stringaricevuta = indalclient.readLine();
            System.out.println("6 ricevuta la stringa dal cliente: " + stringaricevuta);
            stringamodificata = stringaricevuta.toUpperCase();
            System.out.println("7 invio la stringa modificata al client..");
            outversoclient.writeBytes(stringamodificata + '\n');
            System.out.println("9 SERVER: fine elaborazione.. ciao");
            client.close();
        } 
        catch (Exception e) 
        {
            System.out.println(e.getMessage());
            System.out.println("Errore durante la comunicazione..");
            System.exit(1);
        }
    }

    public static void main(String[] args) 
    {
        SocketServer server = new SocketServer();
        server.attendi();
        server.comunica();
    }
}
