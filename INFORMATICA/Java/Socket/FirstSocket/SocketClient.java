package Socket.FirstSocket;

import java.io.*;
import java.net.*;

public class SocketClient 
{
    String name = "127.0.0.1";
    int porta = 6789;
    DataOutputStream out;
    Socket miosocket;
    BufferedReader tastiera;
    String strutente, strserver;
    BufferedReader indalserver;

    public Socket connetti()
    {
        System.out.println("CLIENT partito in esecuzione");
        try 
        {
            tastiera = new BufferedReader(new InputStreamReader(System.in));
            miosocket = new Socket(name, porta);
            out = new DataOutputStream(miosocket.getOutputStream());
            indalserver = new BufferedReader(new InputStreamReader(miosocket.getInputStream()));
        } 
        catch (UnknownHostException e) 
        {
           System.err.println("Host sconosciuto");
           System.exit(1);
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            System.out.println("Errore durante la connessione");
            System.exit(1);
        }
        return miosocket;
    }

    public void comunica()
    {
        try 
        {
            System.out.println("4.. Inserisci la stringa..");
            strutente = tastiera.readLine();
            System.out.println("5.. invio la stringa al server..");
            out.writeBytes(strutente + '\n');
            strserver = indalserver.readLine();
            System.out.println("Stringa ricevuta dal server: " + strserver);
            System.out.println("9.. CLIENT termina l'elaborazione..");
            miosocket.close();
        } 
        catch (Exception e) 
        {
            System.out.println(e.getMessage());
            System.out.println("Errore durante la comunicazione");
            System.exit(1);
        }
    }

    public static void main(String[] args) 
    {
        SocketClient client = new SocketClient();
        client.connetti();
        client.comunica();
    }
}
