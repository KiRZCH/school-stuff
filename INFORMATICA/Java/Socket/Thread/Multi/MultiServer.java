package Socket.Thread.Multi;

import java.net.*;
import java.io.*;

class ServerThread extends Thread
{
    ServerSocket server;
    Socket client;
    String strric;
    String strmod;
    BufferedReader in;
    DataOutputStream out;
    public ServerThread (Socket socket)
    {
        this.client = socket;
    }
    
    public void comunica () throws Exception
    {
        in = new BufferedReader(new InputStreamReader (client.getInputStream()));
        out = new DataOutputStream(client.getOutputStream());

        for (;;)
        {
            strric = in.readLine();
            if (strric.equals(null) || strric.equals("FINE") || strric.equals("fine"))
            {
                out.writeBytes(strric + " {--> Server in chiusura...}" + "\n");
                System.out.println("Echo sul server in chiusura: " + strric);
                break;
            }
            else
            {
                out.writeBytes(strric.toUpperCase() + " (Ricevuta e ritrasmessa)" + "\n");
                System.out.println("6. Echo sul server: " + strric);
            }
        }
        out.close();
        in.close();
        System.out.println("9. Chiusura Socket " + client);
        client.close();
    }

    public void run()
    {
        try 
        {
            comunica();
        } 
        catch (Exception e) 
        {
            e.printStackTrace(System.out);
        }
    }

}

public class MultiServer
{
    public void start()
    {
        try 
        {
            ServerSocket serverSocket = new ServerSocket(6789);
            for (;;)
            {
                System.out.println("1. Server in attesa...");
                Socket socket = serverSocket.accept();
                System.out.println("3. Server " + socket);
                ServerThread serverThread = new ServerThread(socket);
                serverThread.start();
            }
        } 
        catch (Exception e) 
        {
            System.out.println(e.getMessage());
            System.out.println("Errore durante l'istanza del server");
            System.exit(1);    
        }
    }

    public static void main(String[] args) 
    {
        MultiServer tcpServer = new MultiServer();
        tcpServer.start();
    }
}