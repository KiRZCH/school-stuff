package Socket.Thread.Multi;

import java.net.*;
import java.io.*;

public class MultiClient 
{
    Socket client;
    BufferedReader in;
    DataOutputStream out;
    BufferedReader tastiera;
    String strutente, strserver;

    public Socket connetti() throws UnknownHostException, Exception
    {
        System.out.println("2. Client in esecuzione..");
        tastiera = new BufferedReader(new InputStreamReader(System.in));
        client = new Socket(InetAddress.getLocalHost(), 6789);
        out = new DataOutputStream(client.getOutputStream());
        in = new BufferedReader(new InputStreamReader(client.getInputStream()));
        return client;
    }

    public void comunica() throws IOException, Exception
    {
        for (;;)
        {
            System.out.println("4.. Utente, inserisci la stringa da trasmettere: ");
            strutente = tastiera.readLine();
            System.out.println("5.. Invio la stringa");
            out.writeBytes(strutente + "\n");
            strserver = in.readLine();
            System.out.println("7.. risposta dal server " + strserver);
            if (strutente.equals("FINE") || strutente.equals("fine"))
            {
                System.out.println("8.. CLIENT: Termina elaborazione e chiude la connessione");
                client.close();
                break;
            }
        }
    }

    public static void main(String[] args) throws UnknownHostException, IOException, Exception
    {
        MultiClient client = new MultiClient();
        client.connetti();
        client.comunica();
    }

}
