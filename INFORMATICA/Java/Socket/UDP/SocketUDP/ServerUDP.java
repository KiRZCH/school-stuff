package Socket.UDP.SocketUDP;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class ServerUDP 
{
    public static void main(String[] args) throws Exception
    {
        DatagramSocket ServerSocket = new DatagramSocket(6789);
        boolean attivo = true;
        byte[] bufferIN = new byte[1024];
        byte[] bufferOUT = new byte[1024];

        System.out.println("SERVER avviato...");
        while (attivo) 
        {
            DatagramPacket receivPacket = new DatagramPacket(bufferIN, bufferIN.length);
            ServerSocket.receive(receivPacket);
            String ricevuto = new String(receivPacket.getData());
            int numCaratteri = receivPacket.getLength();
            ricevuto = ricevuto.substring(0, numCaratteri);
            System.out.println("RICEVUTO: " + ricevuto);

            InetAddress IPClient = receivPacket.getAddress();
            int portaClient = receivPacket.getPort();

            String daSpedire = ricevuto.toUpperCase();
            bufferOUT = daSpedire.getBytes();

            DatagramPacket sendPacket = new DatagramPacket(bufferOUT, bufferOUT.length, IPClient, portaClient);
            ServerSocket.send(sendPacket);

            if (ricevuto.equals("fine")) 
            {
                System.out.println("SERVER IN CHIUSURA...");
                attivo = false;    
            }
        }
        ServerSocket.close();
    }    
}
