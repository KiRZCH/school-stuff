package Socket.UDP.SocketUDP;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class ClientUDP 
{
    public static void main(String[] args) throws Exception
    {
        int portaServer = 6789;
        InetAddress IPServer = InetAddress.getByName("localhost");

        byte[] bufferOUT = new byte[1024];
        byte[] bufferIN = new byte[1024];
        BufferedReader Input = new BufferedReader(new InputStreamReader(System.in));
        
        DatagramSocket clientSocket = new DatagramSocket();
        System.out.println("Client pronto - inserisci un dato da inviare");

        String daSpedire = Input.readLine();
        bufferOUT = daSpedire.getBytes();

        DatagramPacket sendPacket = new DatagramPacket(bufferOUT, bufferOUT.length, IPServer, portaServer);
        clientSocket.send(sendPacket);

        DatagramPacket receivPacket = new DatagramPacket(bufferIN, bufferIN.length);
        clientSocket.receive(receivPacket);
        String ricevuto = new String(receivPacket.getData());
        int numCaratteri = receivPacket.getLength();
        ricevuto = ricevuto.substring(0, numCaratteri);
        System.out.println("dal SERVER: " + ricevuto);

        clientSocket.close();
    }
}
