package Socket.UDP.datetime;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DTServer 
{
    boolean attivo = true;

    public void comunica() throws Exception
    {
        DatagramSocket ServerSocket = new DatagramSocket(6789);
        byte[] bufferIN = new byte[1024];
        byte[] bufferOUT = new byte[1024];

        System.out.println("SERVER avviato....");
        while(attivo)
        {
            DatagramPacket receivPacket = new DatagramPacket(bufferIN, bufferIN.length);
            ServerSocket.receive(receivPacket);
            String ricevuto = new String(receivPacket.getData());
            ricevuto = ricevuto.substring(0, receivPacket.getLength());
            System.out.println("RICEVUTO: " + ricevuto);

            InetAddress IPClient = receivPacket.getAddress();
            int portaClient = receivPacket.getPort();

            LocalDateTime DT = LocalDateTime.now();
            System.out.println("Ora attuale: " + DT);
            DateTimeFormatter DT_Format = DateTimeFormatter.ofPattern("dd-MM-yyyy | HH:mm:ss");
            String DT_Formatted = DT.format(DT_Format);
            System.out.println("Data formattata: " + DT_Formatted);

            String daSpedire = DT_Formatted;
            bufferOUT = daSpedire.getBytes();

            DatagramPacket sendPacket = new DatagramPacket(bufferOUT, bufferOUT.length, IPClient, portaClient);
            ServerSocket.send(sendPacket);

            if (ricevuto.equals("fine") || ricevuto.equals("FINE"))
            {
                System.out.println("SERVER IN CHIUSURA..");
                attivo = false;
            }
        }
        ServerSocket.close();
    }
    
    public static void main(String[] args) throws Exception 
    {
        DTServer udp_server = new DTServer();
        udp_server.comunica();
    }
}
