package Socket.UDP.datetime;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class DTClient 
{
    int portaServer = 6789;
    
    public void comunica() throws Exception
    {
        InetAddress IPServer = InetAddress.getByName("localhost");
        byte[] bufferOUT = new byte[1024];
        byte[] bufferIN = new byte[1024];
        BufferedReader Input = new BufferedReader(new InputStreamReader(System.in));

        DatagramSocket clientSocket = new DatagramSocket();
        System.out.println("Client pronto - inserisci qualsiasi cosa per ricevere la data attuale");
        String daSpedire = Input.readLine();
        bufferOUT = daSpedire.getBytes();

        DatagramPacket sendPacket = new DatagramPacket(bufferOUT, bufferOUT.length, IPServer, portaServer);
        clientSocket.send(sendPacket);

        DatagramPacket receivPacket = new DatagramPacket(bufferIN, bufferIN.length);
        clientSocket.receive(receivPacket);
        String ricevuto = new String(receivPacket.getData());
        ricevuto = ricevuto.substring(0, receivPacket.getLength());
        System.out.println("dal SERVER: " + ricevuto);

        clientSocket.close();
    }

    public static void main(String[] args) throws Exception
    {
        DTClient udp_client = new DTClient();
        udp_client.comunica();
    }
}
