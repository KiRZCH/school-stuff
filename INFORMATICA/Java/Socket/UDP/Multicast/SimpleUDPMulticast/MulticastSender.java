package Socket.UDP.Multicast.SimpleUDPMulticast;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class MulticastSender 
{
    int port = 6789;
    String IP = "230.1.1.1";

    public void comunica() throws Exception
    {
        DatagramSocket udpSocket = new DatagramSocket();

        InetAddress group = InetAddress.getByName(IP);
        String ciao = "Ciao";
        byte[] msg = ciao.getBytes();
        DatagramPacket packet = new DatagramPacket(msg, msg.length);
        packet.setAddress(group);
        packet.setPort(port);
        udpSocket.send(packet);

        System.out.println("Mandato un messaggio Multicast");
        System.out.println("Chiusura...");
        udpSocket.close();
    }

    public static void main(String[] args) throws Exception
    {
        MulticastSender sender = new MulticastSender();
        sender.comunica();
    }
}
