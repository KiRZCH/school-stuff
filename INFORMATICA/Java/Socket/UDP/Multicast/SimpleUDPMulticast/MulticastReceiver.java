package Socket.UDP.Multicast.SimpleUDPMulticast;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class MulticastReceiver 
{
    int port = 6789;
    String IP = "230.1.1.1";
    
    public void comunica() throws Exception
    {
        MulticastSocket socket = new MulticastSocket(port);
        InetAddress group = InetAddress.getByName(IP);
        System.out.println("Multicast Receiver Inizializzato.. " + socket.getLocalSocketAddress());
        socket.joinGroup(group);

        DatagramPacket packet = new DatagramPacket(new byte[1024], 1024);

        System.out.println("Aspetto il Sender..");
        socket.receive(packet);
        String msg = new String(packet.getData(), packet.getOffset(), packet.getLength());
        System.out.println("[Multicast Receiver] Ricevuto: " + msg);
        socket.leaveGroup(group);
        socket.close();
    }

    public static void main(String[] args) throws Exception
    {
        MulticastReceiver receiver = new MulticastReceiver();
        receiver.comunica();
    }
}
