package Socket.UDP.Multicast.Aforismi;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;

public class AfoServer 
{
    int port = 6789;
    String IP = "230.1.1.1";

    public void comunica() throws Exception
    {
        DatagramSocket udpSocket = new DatagramSocket();
        InetAddress group = InetAddress.getByName(IP);
        Scanner sc = new Scanner(System.in);
        String[] aforismi = new String[4];
        DatagramPacket packet;
        boolean active = true;
        while (active)
        {
            for (int i = 0; i < aforismi.length; i++)
            {
                System.out.print("Inserire il primo aforisma: ");
                aforismi[i] = sc.nextLine();
            }
            for (String string : aforismi) 
            {
                packet = new DatagramPacket(string.getBytes(), string.getBytes().length, group, port);
                udpSocket.send(packet);
                System.out.println("Aforisma Sender Shubel-Kuzenfaust V1.2.4+1: " + string);
            }
            sc.close();
            DatagramPacket rPacket = new DatagramPacket(new byte[1024], 1024);
            udpSocket.receive(rPacket);
            String msg = new String(rPacket.getData(), rPacket.getOffset(), rPacket.getLength());
            System.out.println("Il messaggio '" + msg + "' dal client è stato ricevuto!");
            if (msg.equals("terminato") || msg.equals("Terminato") || msg.equals("TERMINATO"))
                {
                    active = false;
                    System.out.println("Chiusura..");
                }
        }
        udpSocket.close();
    }

    public static void main(String[] args) throws Exception
    {
        AfoServer sender = new AfoServer();
        sender.comunica();
    }
}
