package Socket.UDP.Multicast.Aforismi;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.Scanner;

public class AfoClient 
{
    int port = 6789;
    String IP = "230.1.1.1";
    
    public void comunica() throws Exception
    {
        MulticastSocket socket = new MulticastSocket(port);
        InetAddress group = InetAddress.getByName(IP);
        System.out.println("Ricevo aforismi: ");
        socket.joinGroup(group);
        DatagramPacket packet = new DatagramPacket(new byte[1024], 1024);
        System.out.println("Aspetto...");
        Scanner sc = new Scanner(System.in);
        String[] aforismi = new String[4];
        for (String string : aforismi) 
        {
            socket.receive(packet);
            string = new String(packet.getData(), packet.getOffset(), packet.getLength());
            System.out.println("Aforisma Receiver Shubel-Kuzenfaust V1.7.23+12: " + string);
        }
        String send = "";
        System.out.print("Inserire un messaggio da inviare al server: ");
        send = sc.nextLine();
        DatagramPacket sendPacket = new DatagramPacket(send.getBytes(), send.getBytes().length, group, port);
        socket.send(sendPacket);
        System.out.println("Il messaggio: '" + send + "' e' stato spedito!");
        System.out.println("Chiusura..");
        socket.leaveGroup(group);
        socket.close();
    }

    public static void main(String[] args) throws Exception
    {
        AfoClient receiver = new AfoClient();
        receiver.comunica();    
    }
}
