package Socket.SockyCalc;

import java.io.*;
import java.net.*;
import java.util.Scanner;

public class SockyCalcClient 
{
    Socket client;
    int port = 6789;
    Scanner sc = new Scanner(System.in);
    DataInputStream in;
    DataOutputStream out;

    public Socket connetti() throws IOException
    {
        client = new Socket(InetAddress.getLocalHost(), port);
        in = new DataInputStream(client.getInputStream());
        out = new DataOutputStream(client.getOutputStream());
        return client;
    }

    public void comunica() throws IOException
    {
        while (true)
        {
            System.out.print("Immetti l'equazione: ");
            String eq = sc.nextLine();
            if (eq.equals("end"))
                break;
            out.writeUTF(eq);
            String ris = in.readUTF();
            double risd = Double.parseDouble(ris);
            int risi = (int) Math.round(risd);
            if (risd % 2 == 0)
            {
                System.out.println("Risultato: " + risi);
            }
            else
            {
                System.out.println("Risultato: " + ris);
            }
        }
    }

    public static void main(String[] args) throws IOException
    {
        SockyCalcClient client = new SockyCalcClient();
        client.connetti();
        client.comunica();
    }
}
