package Socket.SockyCalc;

import java.io.*;
import java.net.*;
import java.util.StringTokenizer;

public class SockyCalcServer 
{
    ServerSocket server;
    Socket client;
    int port = 6789;
    DataInputStream in;
    DataOutputStream out;

    public Socket attendi() throws IOException
    {
        server = new ServerSocket(port);
        client = server.accept();
        in = new DataInputStream(client.getInputStream());
        out = new DataOutputStream(client.getOutputStream());
        return client;
    }

    public void comunica() throws IOException
    {
        while (true)
        {
            String input = in.readUTF();
            if (input.equals("end"))
                break;
            System.out.println("Eq ricevuta: " + input);
            double result;
            StringTokenizer st = new StringTokenizer(input);
            double num1 = Double.parseDouble(st.nextToken());
            String op = st.nextToken();
            double num2 = Double.parseDouble(st.nextToken());
            if (op.equals("+"))
            {
                result = num1 + num2;
            }
  
            else if (op.equals("-"))
            {
                result = num1 - num2;
            }
            else if (op.equals("*"))
            {
                result = num1 * num2;
            }
            else if (op.equals("^"))
            {
                result = Math.pow(num1, num2);
            }
            else
            {
                result = num1 / num2;
            }
            System.out.println("Mando i risultati");
            out.writeUTF(Double.toString(result));
        }
    }

    public static void main(String[] args) throws IOException
    {
        SockyCalcServer server = new SockyCalcServer();
        server.attendi();
        server.comunica();
    }
}
