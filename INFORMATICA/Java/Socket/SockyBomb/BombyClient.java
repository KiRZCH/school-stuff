package Socket.SockyBomb;

import java.io.*;
import java.net.*;

public class BombyClient 
{
    Socket client;
    int port = 6789;
    DataInputStream in;
    DataOutputStream out;

    public Socket connetti() throws IOException
    {
        client = new Socket(InetAddress.getLocalHost(), port);
        in = new DataInputStream(client.getInputStream());
        out = new DataOutputStream(client.getOutputStream());
        return client;
    }

    public void comunica() throws IOException
    {
        System.out.println("Aspetto la bomba!");
        int bomba;
        String end = "nend";
        do {
            bomba = in.readInt();
            System.out.println("Ricevo: " + bomba);
            if (bomba == 0 || bomba < 0)
            {
                end = "end";
            }  
            bomba = bomba - 1;
            System.out.println("Rispedisco la bomba al server con " + bomba + " tiri!");
            out.writeInt(bomba);
        } while (end.equals("nend"));
        if (end.equals("end"))
        {
            System.out.println("SONO ESPLOSO");
            close();
        }
    }

    public void close() throws IOException
    {
        System.out.println("Chiudo");
        out.close();
        in.close();
        client.close();
    }

    public static void main(String[] args) throws IOException
    {
        BombyClient client = new BombyClient();
        client.connetti();
        client.comunica();
    }
}
