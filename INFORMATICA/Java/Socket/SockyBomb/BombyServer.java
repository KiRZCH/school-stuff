package Socket.SockyBomb;

import java.io.*;
import java.net.*;
import java.util.Random;

public class BombyServer 
{
    ServerSocket server;
    Socket client;
    int port = 6789;
    DataInputStream in;
    DataOutputStream out;

    public Socket attendi() throws IOException
    {
        server = new ServerSocket(port);
        client = server.accept();
        in = new DataInputStream(client.getInputStream());
        out = new DataOutputStream(client.getOutputStream());
        return client;
    }

    public int creaBomba()
    {
        Random timer = new Random();
        int random = timer.nextInt(100 - 1) + 1;
        return random;
    }

    public void comunica(int bomba) throws IOException
    {
        System.out.println("La bomba è: " + bomba);
        System.out.println("SGANCIO LA BOMBA SUL BERSAGLIO!");
        int ris = bomba;
        do 
        {
            System.out.println("Rispedisco la bomba al client con " + ris + " tiri!");
            out.writeInt(ris);
            ris = in.readInt();
            System.out.println("Ricevo: " + ris);
            ris = ris - 1;
        } while (ris > 0 || ris != 0);
        if (ris == 0 || ris < 0)
        {
            System.out.println("SONO ESPLOSO!");
            close();
        }

    }

    public void close() throws IOException
    {
        System.out.println("Chiudo");
        out.close();
        in.close();
        client.close();
        server.close();
    }

    public static void main(String[] args) throws IOException
    {
        BombyServer server = new BombyServer();
        server.attendi();
        server.comunica(server.creaBomba());
    }
}
