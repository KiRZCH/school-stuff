package StringToBinary;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class StringToBinary 
{
    public static String convertStringToBinary(String input)
    {
        StringBuilder result = new StringBuilder();
        char[] chars = input.toCharArray();
        for (char c : chars) 
        {
            result.append(String.format("%8s", Integer.toBinaryString(c)).replaceAll(" ", "0"));   
        }
        return result.toString();
    }

    public static String prettyBinary(String binary, int blocksize, String separator)
    {
        List<String> result = new ArrayList<>();
        int index = 0;
        while (index < binary.length())
        {
            result.add(binary.substring(index, Math.min(index + blocksize, binary.length())));
            index += blocksize;
        }
        return result.stream().collect(Collectors.joining(separator));
    }

    public static void main(String[] args) 
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Inserire una stringa da convertire in binario");
        String string = in.nextLine();
        String result = convertStringToBinary(string);
        in.close();
        System.out.println(result);
        System.out.println(prettyBinary(result, 8, " "));
    }
}
