package Threads.Loop;

import java.util.Scanner;

public class MainLoopThread {
    
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Inserire il nome del Thread: ");
        String name = in.nextLine();
        System.out.print("Inserire il nome del Thread2: ");
        String name2 = in.nextLine();
        System.out.print("Inserire un valore minimo: ");
        long n1 = in.nextLong();
        System.out.print("Inserire un valore massimo: ");
        long n2 = in.nextLong();
        System.out.print("Inserire la priorità: ");
        int priority = in.nextInt();
        System.out.print("Inserire un valore minimo2: ");
        long n3 = in.nextLong();
        System.out.print("Inserire un valore massimo2: ");
        long n4 = in.nextLong();
        System.out.print("Inserire la priorità2: ");
        int priority2 = in.nextInt();
        LoopThread LP1 = new LoopThread(n1, n2, name, priority);
        LoopThread LP2 = new LoopThread(n3, n4, name2, priority2);
        in.close();
        LP1.start();
        try {
            LP1.join();
        } catch (Exception e) {
            System.out.println(e);
        }
        LP2.start();
        try {
            LP2.join();
        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println("Calcolo completato!");
    }
}
