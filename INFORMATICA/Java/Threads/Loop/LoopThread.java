package Threads.Loop;

public class LoopThread extends Thread {
    private long limiteInferiore, limiteSuperiore;
    private String name = Thread.currentThread().getName();
    private int priority;

    public LoopThread(long limiteInferiore, long limiteSuperiore, String name, int priority)
    {
        this.limiteInferiore = limiteInferiore;
        this.limiteSuperiore = limiteSuperiore;
        this.name = name;
        this.priority = priority;
    }

    public void run()
    {
        setName(name);
        setPriority(priority);
        long liminfper = limiteInferiore * 1000000;
        long limsupper = limiteSuperiore * 1000000;
        for (long i = liminfper; i < limsupper; i++)
        {
            if (i % 500000 == 0)
            {
                System.out.println(name + ": " + i + " Priority level: " + priority);
            }
        }
    }

}
