package Threads.Power.PowerPerPower;

import java.util.Scanner;

public class PowerPerPowerMain
{
    public static void main(String[] args) throws Exception
    {
        Scanner in = new Scanner(System.in);
        System.out.print("Inserire un nome per il Thread1: ");
        String name = in.nextLine();
        System.out.print("Inserire un nome per il Thread2: ");
        String name2 = in.nextLine();
        System.out.print("Inserire un numero da elevare (NON 0): ");
        double n = in.nextDouble();
        PowerPerPowerCalc calc1 = new PowerPerPowerCalc(name, n);
        PowerPerPowerCalc calc2 = new PowerPerPowerCalc(name2, Math.pow(n, 2));
        in.close();
        try {
            if (n == 0)
            {
                throw new Exception("0 alla potenza fa sempre 0, specificare un altro numero");
            }
            calc1.start();
            calc1.join();
            calc2.start();
            calc2.join();
        } catch (Exception e) {
            System.out.println(e);
        } finally
        {
            System.out.println("Operazioni completate!");
        }
    }
}
