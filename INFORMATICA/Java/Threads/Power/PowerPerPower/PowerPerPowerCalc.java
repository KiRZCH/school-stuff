package Threads.Power.PowerPerPower;

public class PowerPerPowerCalc extends Thread {

    String name;
    double n;

    public PowerPerPowerCalc(String name, double n)
    {
        this.name = name;
        this.n = n;
    }
    
    private double simplepower(double n)
    {
        Thread.currentThread().setName(name);
        String namet = Thread.currentThread().getName();
        System.out.println(namet + " dice: ");
        return Math.pow(n, 2);
    }

    public void run()
    {
        System.out.println(simplepower(n));
    }

}
