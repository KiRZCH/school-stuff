package Threads.ContaTroppo;

import java.util.Scanner;

public class ContaTroppoMain 
{
    public static void main(String[] args) 
    {
        Scanner in = new Scanner(System.in);
        System.out.print("Nome del Thread1: ");
        String name1 = in.nextLine();
        System.out.print("Nome del Thread2: ");
        String name2 = in.nextLine();
        System.out.print("Numero a cui arrivare: ");
        int n = in.nextInt();
        ContaTroppoCalc calc1 = new ContaTroppoCalc(n, name1);
        ContaTroppoCalc calc2 = new ContaTroppoCalc(n*5, name2);
        try {
            calc1.start();
            calc1.join();
            calc2.start();
            calc2.join();
        } catch (Exception e) {
            System.out.println(e);
        } finally
        {
            System.out.println("Operations Complete!");
        }
        in.close();
    }
}
