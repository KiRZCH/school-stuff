package Threads.PariDispari;

import java.util.Scanner;

public class MainPD 
{
    public static void main(String[] args) 
    {
        Scanner in = new Scanner(System.in);
        int n;
        System.out.print("Inserire un valore: ");
        n = in.nextInt();
        PariDispari TP = new PariDispari(n + 1, true);
        PariDispari TD = new PariDispari(n + 1, false);
        TP.start();
        TD.start();
        try {
            TP.join();
            TD.join();
        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println("Fine Conteggio");
        in.close();
    }
}
