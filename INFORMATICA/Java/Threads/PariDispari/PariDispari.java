package Threads.PariDispari;

public class PariDispari extends Thread {
    private int massimo;
    private boolean pari;
    private int ritardo = 500;

    public PariDispari (int finale, boolean pari)
    {
        massimo = finale;
        this.pari = pari;
    }

    public void run()
    {
        String chisono;
        chisono = Thread.currentThread().getName();
        for (int xx = 0; xx < massimo; xx++)
        {
            if (pari)
            {
                if (xx % 2 == 0)
                    System.out.println(chisono + " Pari: " + xx);
            } else {
                if (xx % 2 != 0)
                    System.out.println(chisono + " Dispari: " + xx);
            }
            try {
                Thread.sleep(ritardo);
            } catch (InterruptedException e) {
                System.out.println(e);
            }
        } 
    }
}
