package Threads.Tabellina;

public class TabellinaCalc extends Thread 
{
    double n;
    int n2;

    public TabellinaCalc(double n, int n2)
    {
        this.n = n;
        this.n2 = n2;
    }

    private void tabellina(double n, int n2, String name)
    {
        for (int i = 1; i <= n2; i++)
        {
            System.out.println("Tabellina del " + (int)n + ": " + (int)(n * i));
        }
    }

    public void run()
    {
        String name = Double.toString(n);
        Thread.currentThread().setName(name);
        tabellina(n, n2, name);
    }
}
