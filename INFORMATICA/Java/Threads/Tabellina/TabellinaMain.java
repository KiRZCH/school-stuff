package Threads.Tabellina;

import java.util.Scanner;

public class TabellinaMain 
{
    public static void main(String[] args) 
    {
        Scanner in = new Scanner(System.in);
        System.out.print("Inserire il numero a cui fare la tabella (Thread1): ");
        double n = in.nextDouble();
        System.out.print("Inserire il massimo della tabella (Thread1): ");
        int n2 = in.nextInt();
        System.out.print("Inserire il numero a cui fare la tabella (Thread2): ");
        double n3 = in.nextDouble();
        System.out.print("Inserire il massimo della tabella (Thread2): ");
        int n4 = in.nextInt();
        TabellinaCalc calc1 = new TabellinaCalc(n, n2);
        TabellinaCalc calc2 = new TabellinaCalc(n3, n4);
        in.close();
        try {
            calc1.start();
            calc1.join();
            calc2.start();
            calc2.join();
        } catch (Exception e) {
            System.out.println(e);
        } finally
        {
            System.out.println("Operations Complete!");
        }

    }
}
