package Threads.Radius.Circle;
import java.lang.Math;
import java.util.Scanner;

public class RadiusCalculate extends Thread 
{
    private double perimeter(double radius)
    {
        return 2 * Math.PI * radius;
    }

    private double area(double radius)
    {
        return Math.PI * Math.pow(radius, 2);
    }

    public void run()
    {
        setName("Calcolatore");
        System.out.println(Thread.currentThread().getName());
        System.out.print("Inserire il raggio: ");
        Scanner in = new Scanner(System.in);
        double radius = in.nextDouble();
        double perimeter = perimeter(radius);
        double area = area(radius);
        System.out.println("Perimeter = " + perimeter);
        System.out.println("Area = " + area);
        in.close();
    }
}