package Threads.Radius.Rectangle;

import java.util.Scanner;

public class RectangleCalculator extends Thread
{
    private double perimeter(double w, double h)
    {
        return 2 * (w + h);
    }

    private double area(double w, double h)
    {
        return h * w;
    }

    @Override
    public void run() 
    {
        setName("Calculator");
        System.out.println(Thread.currentThread().getName());
        Scanner in = new Scanner(System.in);
        System.out.print("Immettere la larghezza: ");
        double w = in.nextDouble();
        System.out.print("Immettere l'altezza: ");
        double h = in.nextDouble();
        double perimeter = perimeter(w, h);
        double area = area(w, h);
        System.out.println("Perimeter = " + perimeter);
        System.out.println("Area = " + area);
        in.close();
    }
}
