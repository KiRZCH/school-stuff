package Threads.Polygon;

import java.lang.Math;
import java.util.Scanner;

public class PolygonCalc extends Thread
{
    private double calc(int n, double s)
    {
        return (n * Math.pow(s, 2) / (4 * Math.tan(Math.PI/n)));
    }

    public void run()
    {
        setName("PolygonCalc");
        System.out.println(Thread.currentThread().getName() + " is running!");
        System.out.print("Input the number of the sides of the polygon: ");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        System.out.print("Input the number of one of the sides: ");
        double s = in.nextDouble();
        double calc = calc(n, s);
        System.out.println("The area of the polygon is: " + calc);
        in.close();
    }
}
