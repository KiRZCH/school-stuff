package Threads.Hexagon;

import java.lang.Math;
import java.util.Scanner;

public class HexaCalc extends Thread
{
    private double calc(double s)
    {
        return (6 * Math.pow(s, 2) / (4 * Math.tan(Math.PI / 6)));
    }

    public void run()
    {
        setName("HexaCalc");
        System.out.println(Thread.currentThread().getName() + " is running!");
        Scanner in = new Scanner(System.in);
        System.out.print("Input the lenght of a side of the hexagon: ");
        double s = in.nextDouble();
        double calc = calc(s);
        System.out.println("The area of the hexagon is: " + calc);
        in.close();
    }
}
