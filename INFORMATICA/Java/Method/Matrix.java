package Method;

import java.util.Scanner;

public class Matrix 
{
    public static void matrix(int n, int s)
    {
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                System.out.print((int)(Math.random() * s) + " ");
            }
            System.out.println();
        }
    }
    
    public static void main(String[] args) 
    {
        Scanner in = new Scanner(System.in);
        System.out.print("Metti un numero: ");
        int n = in.nextInt();
        System.out.print("Metti il numero massimo: ");
        int s = in.nextInt();
        in.close();
        matrix(n, s);    
    }
}
