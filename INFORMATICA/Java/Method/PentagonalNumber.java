package Method;

import java.util.Scanner;

public class PentagonalNumber 
{
    public static void main(String[] args) 
    {
        int count = 1;
        System.out.print("Inserire il numero a cui arrivare: ");
        Scanner in = new Scanner(System.in);
        int number = in.nextInt();
        for (int i = 0; i <= number; i++)
        {
            System.out.printf("%-6d", getPentagonalNumber(i));
            if (count % 10 == 0)
                System.out.println();
            count++;
        }
        in.close();
    }

    public static int getPentagonalNumber(int i)
    {
        return (i * (3 * i - 1)) / 2;
    }
}
