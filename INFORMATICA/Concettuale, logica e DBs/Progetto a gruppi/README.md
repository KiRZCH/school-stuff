# SQL ISLAND : **JOIN** TOGETHER!

> Autori: Riccardo Bazzichi, Gabriele Del Balio, Emanuele Manfredini, Thierry Mugnaini, Francesco Usai.

[[_TOC_]]

## Descrizione del progetto

### Richiesta

>La **K Hi-Tech Videogames and Software Solutions s.r.l s.p.a co. limited. united & Sons** ha perso le speranze, a quanto pare nessuno riesce a creare un database per il loro nuovo gioco.
>L'unica speranza è un gruppetto della 5BTech. \
>La K.. ecc.. ecc.. & Sons ha bisogno di un giocatore, completo di tutti i dati che si possano chiedere durante un login e una registrazione, di un posticino dove registrare i propri giochi,
>in più, sappiamo che il gioco è un *MMORPGSQL Based*, e che il gioco è hostato su diversi server, e che è possibile controllarlo tramite un codice dato ad ogni hosting, vogliono sapere l'IP e l'orario **atomicamente** preciso della connessione
>del giocatore al server, se non bastasse questo, nel gioco il giocatore ha anche un inventario semplice, basato esclusivamente sul numero di spazio disponibile, occupato e totale.
>Ovviamente non finisce qui perchè all'interno del server ci sono tanti realm tra cui scegliere e ognuno di essi ha tante zone caratterizzate da biomi tipo foresta, inverno, spiaggia, sito nucleare ecc.. \
>La **K Hi-Tech Videogames and Software Solutions s.r.l s.p.a co. limited. united & Sons** vi ringrazia! \
>*May The Force Be With You All!*

Si richiede di realizzare un esercizio di progettazione di un Database (db)
con le seguenti caratteristiche:

1. [x] **Almeno 4 entità** 

2. [x] **Almeno 3 relazioni**

	1. [x] Di cui almeno una **1:N** e una **N:N**

### Proposta

Data la seguente richiesta, abbiamo sviluppato questa proposta di progetto:

1. [x] **6 Entità**

2. [x] **6 Relazioni**

	1. [x] Più di una **1:N** e di una **N:N**


## Il Nostro Gioco

Grazie all'impegno di 5 ingegni della ***New Gen*** la **K Hi-Tech Videogames and Software Solutions s.r.l s.p.a co. limited. united & Sons** è orgogliosa di presentarvi il loro nuovo capolavoro videoludico. \
Nella bella **Isola SQL** un eroe sta per tornare per evitare la corruzione del **Island DBMS**, nonchè il cuore dell'isola, ma non può farcela da solo, dovrà unire le forze insieme a gli avventurieri di tutto il mondo e ripristinare la pace sull'Isola SQL.

### Un Nuovo Motore Grafico

I tuoi occhi brilleranno dinanzi ad **Unreal SQL Creator 5**, il nuovo Engine grafico della **BabyCryNet** in grado di aprire i tuoi occhi a nuove emozioni!

### Una storia... LA storia!

Immergiti nei dialoghi a son di **SELECT** con i nuovi NPC di **"SQL ISLAND : JOIN TOGETHER!"**, una storia tutta da scoprire e da digitare!

### Nemici?! DROP!

Non aver paura degli avversari, alla fine.. basta un **DROP**!

### Co-Op / Multiplayer

Fino a 4 giocatori a schermo condiviso[^1] e fino a **20** giocatori online da tutto il mondo.. ti senti solo? beccate sta lobby!

### C and Derivates 100% FREE

***Questo gioco è interamente sviluppato in SQL***[^2], niente C, C++, Java, C# o altri linguaggi di programmazione. Perchè aspettare un compilatore?! ci mette troppo!

## Making Of

### Concettuale

#### Immagine

![Concettuale](img/Lavoro a Gruppi.drawio.svg)

#### Links

[Google Drive](https://drive.google.com/file/d/1qN3SN4plXqX8QTQMYL31QkbXiS4p-vz5/view?usp=sharing "Google Drive")

[Draw.io](https://viewer.diagrams.net/?tags=%7B%7D&highlight=0000ff&edit=_blank&layers=1&nav=1#G1qN3SN4plXqX8QTQMYL31QkbXiS4p-vz5 "Draw.io Preview")

### Logica

Giochi(**ID**, Nome, Autore) \
Giocatori(**Nickname**[^3], email, Password, IP_di_registazione) \
Gioca(**Nickname**, **ID_Gioco**) \
Inventari(**ID**[^4], Spazio_Totale, Spazio_Disponibile, Spazio_Occupato) \
Server(**ID**, Nome, Regione, IP) \
Connessione(**Nickname**, **ID_Server**, IP_di_connessione, Timestamp) \
Realm(**ID**, Nome, <i>ID_server</i>) \
Zone(**ID**, Nome, Bioma, <i>ID_Realm</i>)

### SQL DB

Link: [Lavoro a gruppi](DB/Lavoro_a_gruppi.db "Scarica il Database")

#### SQL Queries

##### SELECT

```sql
--Seleziona tutti i Giochi
SELECT * FROM Giochi WHERE id = 0;

--Seleziona tutti i giocatori
SELECT * FROM Giocatori WHERE id = 0;

--Seleziona il nickname con la corrispondente password criptata
SELECT Nickame FROM Giocatori WHERE password = sha512("ciaomondo");

--Seleziona la password dei nickname che terminano con "Omar"
SELECT password FROM Giocatori WHERE Nickname LIKE "%Omar";

--Seleziona gli IP dai server con ID = o superiori a 0
SELECT IP FROM Server WHERE id >= 0;

--Ve la spiego a voce, faccio prima :)
SELECT Server.IP, Hosta.ID_Hosting, Giochi.Nome FROM Hosta, Server, Giochi WHERE Hosta.ID_Server = Server.ID AND Hosta.ID_Gioco = Giochi.ID;
```

##### INSERT

```sql
--Creiamo un Gioco
INSERT INTO Giochi(ID, Nome, Autore) VALUES (1, "Prince Of China", "Chin Chan-Pai Soft");

--Creiamo un Giocatore
INSERT INTO Giocatori(Nickname, email, password, IP_di_registrazione) VALUES ("Er Kappa", "kappak@5bt.gg", sha512("kappa"), "19.0.0.2");

--Creiamo una tabella di relazione tra Giocatore e Giochi
INSERT INTO Gioca(Nickname, ID_Gioco) VALUES ("Er Kappa", 1);

--Creiamo un Inventario
INSERT INTO Inventario(ID, Spazio_Totale, Spazio_Disponibile, Spazio_Occupato) VALUES ("Er Kappa", 220, 200, 20);

--Creiamo un Server
INSERT INTO Server(ID, Nome, Regione, IP) VALUES(1, "China's Government", "ASIA", "222.222.222.222");

--Creiamo una tabella di relazione tra Server e Giocatori
INSERT INTO Connessioni(Nickname, ID_Server, IP_di_connessione, Timestamp) VALUES ("Er Kappa", 1, "222.111.50.25", "2012-12-12 12:12:12");
```
[^5]


## Note

[^1]: Lo schermo condiviso è esclusivo per proprietari di KStation e KBOX 

[^2]: In questo progetto abbiamo utilizzato SQLite

[^3]: Abbiamo optato per Nickname come chiave primaria, dato che nel gioco il Nickname è univoco

[^4]: In questo caso ID è sia chiave primaria che chiave esterna, riferendosi al Nickname del giocatore che possiede l'inventario

[^5]: Usiamo una funzione di Hash (SHA512) per criptare la password.
