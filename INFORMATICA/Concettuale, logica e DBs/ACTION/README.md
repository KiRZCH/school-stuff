# ACTION!

> Gabriele Del Balio - 5BT

## Es 1.

### Concettuale

#### Links

- [Draw.io](https://viewer.diagrams.net/?tags=%7B%7D&highlight=0000ff&edit=_blank&layers=1&nav=1#G1kAPPLVkySPnTyWZc4cr8ED7m8-zmPFaE "Originale")
- [Google Drive](https://drive.google.com/file/d/1kAPPLVkySPnTyWZc4cr8ED7m8-zmPFaE/view?usp=sharing "Secondario / Mirror")

#### Immagine:

![Concettuale](Concettuale/ACTION.drawio.svg)

### Logica

```
Attori(CF, Nome, Cognome, Data_Nascita, Luogo_Nascita); [Nota #1]
Produttori(Ragione_Sociale, Anno_Fondazione, Recapito);
Film(Nome, Anno_Produzione, RS_Produttore); [Nota #2]
Ruolo(Nome_Film, CF_Attore, Ruolo);
Valutazioni(RS_Produttore, Nome_Film, Voto); [5 <= Voto <= 10]
```

## Database, SQL Queries AKA Es 2.

#### DB Link:

[ACTION.db](DB/ACTION.db "Database")

#### Es A.

> La creazione di una tabella a scelta.

```sql
CREATE TABLE test 
(
    id int NOT NULL,
    nome varchar(20) NOT NULL,
    cognome varchar(20) NOT NULL,
    anno_nascita date NOT NULL,
    PRIMARY KEY(id)
);
```

#### Es B.

> La cancellazione di una tabella a scelta.

```sql
DROP TABLE test;
```

#### Es C.

> L’aggiunta del campo “genere” nella tabella che ritenete significativa per questo cambiamento

```sql
ALTER TABLE film ADD COLUMN genere varchar(50)
```

#### Es D.

> l’inserimento di un record esemplificativo in una tabella

```sql
INSERT INTO attori(CF, nome, cognome, data_nascita, luogo_nascita) VALUES ("PTTBRD00A01F205V", "Brad", "Pitt", "01-01-1900", "Milano")
```

#### Es E.

> la lista dei film in cui l’attore Brad Pitt recita come protagonista.

```sql
SELECT film.nome from film, ruolo, attori WHERE film.nome = ruolo.nome_film AND attori.CF = ruolo.CF_attore AND ruolo.ruolo = "Protagonista" AND attori.nome = "Brad" AND attori.cognome = "Pitt";
```

Risultato

> Die Soft: Facili a Morire

### Note
```
Nota #1: Ho optato per il Codice Fiscale come chiave primaria, pensando sia un database esclusivo agli attori italiani

Nota #2: Dato che nessun film avrà mai lo stesso nome, ho optato "Nome" come chiave primaria

```