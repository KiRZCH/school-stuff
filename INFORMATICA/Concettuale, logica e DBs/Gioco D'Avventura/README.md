# Gioco D'Avventura

> Gabriele Del Balio - Esercizio di Verifica
> Classe 5<sup>a</sup>BT - 17/03/2022 CEST

## Database Link

Database Type: **wxSQLite3** <br>
Database Encryption Type: **AES 256-bit** <br>
Database Password: **1234** <br>
Database File: [Link](DB/Gioco.db "Link al Database") <br>
Database SQL File for direct execution: [Link](DB/Gioco.sql "Link al file SQL") <br>
[^1]

## Database Code

```sql
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: Acquisire
DROP TABLE IF EXISTS Acquisire;

CREATE TABLE Acquisire (
    ID             INTEGER      PRIMARY KEY,
    Nome_Car       VARCHAR (50) REFERENCES Caratteristiche (Nome) ON DELETE CASCADE
                                                                  ON UPDATE CASCADE,
    Nick_Giocatore VARCHAR (20) REFERENCES Giocatori (Nickname) ON DELETE CASCADE
                                                                ON UPDATE CASCADE,
    Timestamp      DATETIME     NOT NULL
);

INSERT INTO Acquisire (
                          ID,
                          Nome_Car,
                          Nick_Giocatore,
                          Timestamp
                      )
                      VALUES (
                          1,
                          'Rutto Fotonico',
                          'Johnnypack',
                          '01-01-2022 22:22:22'
                      );

INSERT INTO Acquisire (
                          ID,
                          Nome_Car,
                          Nick_Giocatore,
                          Timestamp
                      )
                      VALUES (
                          2,
                          'Ululato Stridulante',
                          'Johnnypack',
                          '01-01-2022 22:22:22'
                      );

INSERT INTO Acquisire (
                          ID,
                          Nome_Car,
                          Nick_Giocatore,
                          Timestamp
                      )
                      VALUES (
                          3,
                          'Rutto Fotonico',
                          'Johnnypack',
                          '01-01-2022 22:22:22'
                      );

INSERT INTO Acquisire (
                          ID,
                          Nome_Car,
                          Nick_Giocatore,
                          Timestamp
                      )
                      VALUES (
                          4,
                          'Ululato Stridulante',
                          'Johnnypack',
                          '01-01-2022 22:22:22'
                      );


-- Table: Azioni
DROP TABLE IF EXISTS Azioni;

CREATE TABLE Azioni (
    Nome        VARCHAR (50) PRIMARY KEY,
    Descrizione TEXT         NOT NULL,
    Nome_Stanza VARCHAR (50) REFERENCES Stanze (Nome) ON DELETE CASCADE
                                                      ON UPDATE CASCADE
);

INSERT INTO Azioni (
                       Nome,
                       Descrizione,
                       Nome_Stanza
                   )
                   VALUES (
                       'Salto',
                       'Puoi saltare',
                       'FiorParty'
                   );

INSERT INTO Azioni (
                       Nome,
                       Descrizione,
                       Nome_Stanza
                   )
                   VALUES (
                       'Des, Sinis',
                       'Vai solo a Destra e a Sinistra',
                       'ThrillerWarp'
                   );


-- Table: Caratteristiche
DROP TABLE IF EXISTS Caratteristiche;

CREATE TABLE Caratteristiche (
    Nome        VARCHAR (50) PRIMARY KEY
                             NOT NULL,
    Categoria   VARCHAR (50) NOT NULL,
    Descrizione TEXT         NOT NULL,
    Punteggio   INT          NOT NULL
);

INSERT INTO Caratteristiche (
                                Nome,
                                Categoria,
                                Descrizione,
                                Punteggio
                            )
                            VALUES (
                                'Rutto Fotonico',
                                'Capacità',
                                'Spara un fascio luminoso dalla bocca, accecando e incenerendo i nemici',
                                20
                            );

INSERT INTO Caratteristiche (
                                Nome,
                                Categoria,
                                Descrizione,
                                Punteggio
                            )
                            VALUES (
                                'Ululato Stridulante',
                                'Capacità',
                                'Ulula alla luna e assorda il male',
                                27
                            );


-- Table: Giocatori
DROP TABLE IF EXISTS Giocatori;

CREATE TABLE Giocatori (
    Nickname     VARCHAR (20) PRIMARY KEY
                              NOT NULL,
    Avatar       VARCHAR (20) NOT NULL,
    Data_Nascita DATE         NOT NULL,
    Città        VARCHAR (50) NOT NULL,
    Via          VARCHAR (50) NOT NULL,
    N_Civico     INT          NOT NULL,
    Punteggio    INT          NOT NULL
                              DEFAULT (5) 
);

INSERT INTO Giocatori (
                          Nickname,
                          Avatar,
                          Data_Nascita,
                          Città,
                          Via,
                          N_Civico,
                          Punteggio
                      )
                      VALUES (
                          'Johnnypack',
                          'JPack',
                          '12-03-1997',
                          'Rotterdam',
                          'Rouge',
                          1232,
                          5
                      );


-- Table: Offre
DROP TABLE IF EXISTS Offre;

CREATE TABLE Offre (
    Nome_Car    VARCHAR (50) REFERENCES Caratteristiche (Nome) ON DELETE CASCADE
                                                               ON UPDATE CASCADE,
    Nome_Stanza VARCHAR (50) REFERENCES Stanze (Nome) ON DELETE CASCADE
                                                      ON UPDATE CASCADE,
    PRIMARY KEY (
        Nome_Car,
        Nome_Stanza
    )
);

INSERT INTO Offre (
                      Nome_Car,
                      Nome_Stanza
                  )
                  VALUES (
                      'Rutto Fotonico',
                      'FiorParty'
                  );

INSERT INTO Offre (
                      Nome_Car,
                      Nome_Stanza
                  )
                  VALUES (
                      'Ululato Stridulante',
                      'ThrillerWarp'
                  );


-- Table: Stanze
DROP TABLE IF EXISTS Stanze;

CREATE TABLE Stanze (
    Nome    VARCHAR (50) PRIMARY KEY
                         NOT NULL,
    Aspetto VARCHAR (20) NOT NULL
);

INSERT INTO Stanze (
                       Nome,
                       Aspetto
                   )
                   VALUES (
                       'FiorParty',
                       'Fantasy'
                   );

INSERT INTO Stanze (
                       Nome,
                       Aspetto
                   )
                   VALUES (
                       'ThrillerWarp',
                       'Dark'
                   );


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;

```
<<<<<<< HEAD

## SQL Query

```

```
=======
>>>>>>> c73301db6c39bfb660f9133c417dbf32dbeadf87

## Note

[^1]: Viene utilizzato wxSQLite3 come DBMS, con SQLiteStudio come IDE
