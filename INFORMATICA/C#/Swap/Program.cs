﻿using System;
public class SwapExcercise
{
    public static void Main(string[] args)
    {
        int n1,n2,tmp;
        Console.Write("\nInput the N1 Number: ");
        n1 = int.Parse(Console.ReadLine());
        Console.Write("Input the N2 Number: ");
        n2 = int.Parse(Console.ReadLine());
        tmp = n1;
        n1 = n2;
        n2 = tmp;
        Console.WriteLine("After Swapping: ");
        Console.WriteLine("N1: " + n1);
        Console.WriteLine("N2: " + n2);
        Console.WriteLine("Premere un tasto per uscire...");
        Console.Read();
    }
}