﻿using System;
using System.Threading;

public class CalcolaThread
{
    private double n1, n2;
    private string name = "";

    public CalcolaThread(double n1, double n2)
    {
        this.n1 = n1;
        this.n2 = n2;
    }
    public void basicOPs()
    {
        Console.ForegroundColor = ConsoleColor.Green;
        name = "Operazioni Basiche";
        Thread.CurrentThread.Name = name;
        Console.WriteLine("Starting {0}", Thread.CurrentThread.Name);
        Console.WriteLine("{0} + {1} = {2}", n1, n2, n1+n2);
        Console.WriteLine("{0} - {1} = {2}", n1, n2, n1-n2);
        Console.WriteLine("{0} * {1} = {2}", n1, n2, n1*n2);
        Console.WriteLine("{0} / {1} = {2}", n1, n2, n1/n2);
        Console.WriteLine("{0} finished!", Thread.CurrentThread.Name);
    }

    public void advOPs()
    {
        Console.ForegroundColor = ConsoleColor.Cyan;
        name = "Operazioni Avanzate";
        Thread.CurrentThread.Name = name;
        Console.WriteLine("Starting {0}", Thread.CurrentThread.Name);
        Console.WriteLine("{0}^{1} = {2}", n1, n2, Math.Pow(n1, n2));
        Console.WriteLine("Radice di {0} = {1}", n1, Math.Sqrt(n1));
        Console.WriteLine("Radice di {0} = {1}", n2, Math.Sqrt(n2));
        Console.WriteLine("Logaritmo naturale di {0} = {1}", n1, Math.Log(n1));
        Console.WriteLine("Logaritmo naturale di {0} = {1}", n2, Math.Log(n2));
        Console.WriteLine("Logaritmo di {0} in base {1} = {2}", n1, n2, Math.Log(n1, n2));
        Console.WriteLine("{0} finished!", Thread.CurrentThread.Name);
    }

    public void sicosOps()
    {
        Console.ForegroundColor = ConsoleColor.Red;
        name = "Seno/Coseno/Tangente";
        Thread.CurrentThread.Name = name;
        Console.WriteLine("Starting {0}", Thread.CurrentThread.Name);
        Console.WriteLine("Seno di {0} = {1}", n1, Math.Sin(n1));
        Console.WriteLine("Seno di {0} = {1}", n2, Math.Sin(n2));
        Console.WriteLine("Coseno di {0} = {1}", n1, Math.Cos(n1));
        Console.WriteLine("Coseno di {0} = {1}", n2, Math.Cos(n2));
        Console.WriteLine("Tangente di {0} = {1}", n1, Math.Tan(n1));
        Console.WriteLine("Tangente di {0} = {1}", n2, Math.Tan(n2));
        Console.WriteLine("{0} finished!", Thread.CurrentThread.Name);
    }
}

public class CalcolaMain
{
    public static void Main()
    {
        Console.Clear();
        Console.Write("Immettere n1: ");
        double n1 = Convert.ToDouble(Console.ReadLine());
        Console.Write("Immettere n2: ");
        double n2 = Convert.ToDouble(Console.ReadLine());
        CalcolaThread calc = new CalcolaThread(n1, n2);
        Thread t1 = new Thread(new ThreadStart(calc.basicOPs));
        Thread t2 = new Thread(new ThreadStart(calc.advOPs));
        Thread t3 = new Thread(new ThreadStart(calc.sicosOps));
        try
        {
            Console.WriteLine("---------------------------");
            t1.Start();
            t1.Join();
            Console.WriteLine("---------------------------");
            Console.WriteLine("Premere per continuare...");
            Console.ReadLine();
            Thread.Sleep(1000);
            t2.Start();
            t2.Join();
            Console.WriteLine("---------------------------");
            Console.WriteLine("Premere per continuare...");
            Console.ReadLine();
            Thread.Sleep(1000);
            t3.Start();
            t3.Join();
            Console.WriteLine("---------------------------");
            Thread.Sleep(1000);
        }
        catch (System.Exception e)
        {
            Console.WriteLine(e);
        }
        finally
        {
            Console.WriteLine("All OPs Completed!");
            Console.WriteLine("Premere un tasto per chiudere...");
            Console.ReadLine();
        }
    }
}