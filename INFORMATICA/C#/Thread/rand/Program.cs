﻿using System;
using System.Threading;
using System.Diagnostics;

class join
{
    static void random()
    {
        Console.ForegroundColor = ConsoleColor.Green;
        Random r = new Random();
        Console.WriteLine("{0} dice: {1,15:N0}", Thread.CurrentThread.ManagedThreadId, (long)r.Next(Int32.MaxValue) + (long)r.Next(Int32.MaxValue));
    }

    static void Main()
    {
        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();
        Random rand = new Random();
        int n = rand.Next(1001);
        Thread[] t_array = new Thread[n];
        try
        {
            for (int i = 0; i < t_array.Length; i++)
            {
                t_array[i] = new Thread(new ThreadStart(random));
                t_array[i].Start();
            }
            for (int i = 0; i < t_array.Length; i++)
            {
                t_array[i].Join();
            }
        }
        catch (System.Exception)
        {
            Console.WriteLine("Errore!");
            throw;
        } 
        finally
        {
            TimeSpan ts = stopwatch.Elapsed;
            string tempo = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine("Tempo di elaborazione: {0}", tempo);
            Console.WriteLine("Premere per uscire...");
            Console.Read();
        }
    }
}