﻿using System;
public class TNM
{
    public static void Main(string[] args)
    {
        double n1,n2,n3;
        Console.WriteLine("Inserire N1, N2 e N3 in modo da moltiplicarli.");
        Console.Write("Inserire N1: ");
        n1 = double.Parse(Console.ReadLine());
        Console.Write("Inserire N2: ");
        n2 = double.Parse(Console.ReadLine());
        Console.Write("Inserire N3: ");
        n3 = double.Parse(Console.ReadLine());
        double tot = n1 * n2 * n3;
        Console.WriteLine("Totale: " + n1 + " x " + n2 + " x " + n3 + " = " + tot);
        Console.WriteLine("Premere un pulsante per chiudere...");
        Console.Read();
    }
}