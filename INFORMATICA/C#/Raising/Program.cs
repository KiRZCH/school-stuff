﻿using System;

namespace Excersises
{
    public class Rising 
    {
        public static void Main(string[] args)
        {
            Console.Clear();
            Console.Write("Inserire il numero da elevare: ");
            double x = Convert.ToDouble(Console.ReadLine());
            Console.Write("Inserire la potenza: ");
            double n = Convert.ToDouble(Console.ReadLine());
            Rising calc = new Rising();
            calc.Riser(x, n);
            Console.ReadLine();
        }

        private void Riser(double x, double n)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            for (int power = 0; power <= n; power++)
                Console.WriteLine("{0}^{1} = {2:N0} (0x{3:X})", x, power, (long)Math.Pow(x, power), (long)Math.Pow(x, power));
            Console.Write("Premere un tasto per chiudere...");
            Console.Read();
        }
    }
}