﻿using System;

namespace Excersise
{
    public class LnSemiLoop
    {
        public static void Main(string[] args)
        {
            Console.Clear();
            Console.Write("Inserire il numero (max: {0}): ", double.MaxValue);
            double x = Convert.ToDouble(Console.ReadLine());
            Console.Write("Inserire quante volte usare il logaritmo (max: {0}): ", int.MaxValue);
            int n = Convert.ToInt32(Console.ReadLine());
            LnSemiLoop ln = new LnSemiLoop();
            ln.LnCalc(x, n);
        }

        private void LnCalc(double x, int n)
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            double x2 = x;
            try
            {
                if (x2 < 0)
                    throw new Exception();
                if (n < 0 || n == 0)
                    throw new Exception();
                 for (int i = 0; i < n; i++)
                 {
                    Console.WriteLine("[{2}]Logaritmo naturale di {0} = {1}", x, Math.Log(x), i + 1);
                    x = Math.Log(x);
                    if (x2 == 0)
                        break;
                    if (x < 0)
                        break;
                 }
            }
            catch (System.Exception e) when (x2 < 0 || x == double.NaN)
            {
                throw new Exception("The Ln fuction is not defined for < 0", e);
            }
            catch (System.Exception e) when (n < 0 || n == 0)
            {
                throw new Exception("Cannot set N as negative or 0", e);
            }
            finally
            {
                Console.WriteLine("Operations Complete!");
                Console.Write("Premere un tasto per chiudere...");
                Console.Read();
            }
        }
    }
}