﻿using System;
public class Calc
{
    public static void Main(string[] args)
    {
        double n1, n2;
        Console.WriteLine("Inserire 2 numeri...");
        Console.Write("N1: ");
        n1 = double.Parse(Console.ReadLine());
        Console.Write("N2: ");
        n2 = double.Parse(Console.ReadLine());
        Console.WriteLine(n1 + " + " + n2 + " = " + (n1+n2));
        Console.WriteLine(n1 + " - " + n2 + " = " + (n1-n2));
        Console.WriteLine(n1 + " x " + n2 + " = " + (n1*n2));
        Console.WriteLine(n1 + " / " + n2 + " = " + (n1/n2));
        Console.WriteLine(n1 + " ^ " + n2 + " = " + Math.Pow(n1,n2));
        Console.WriteLine("Log" + n1 + "(" + n2 + ")" + " = " + Math.Log(n1, n2));
        Console.WriteLine(n1 + " mod " + n2 + " = " + n1%n2);
        Console.Write("Premere un tasto per chiudere...");
        Console.Read();
    }
}