﻿using System;

namespace Excersise
{
    public class Derising
    {
        public static void Main(string[] args)
        {
            Console.Clear();
            Console.Write("Inserire il valore da radicare (max: {0}): ", double.MaxValue);
            double x = Convert.ToDouble(Console.ReadLine());
            Console.Write("Inserire quante volte radicarlo (max: {0}): ", int.MaxValue);
            int n = Convert.ToInt32(Console.ReadLine());
            Derising calc = new Derising();
            calc.deraisingCalc(x, n);
        }

        private void deraisingCalc(double x, int n)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            try
            {
                if (n < 0 || n == 0)
                {
                    throw new Exception();
                }
                if (x < 0)
                {
                   throw new Exception();
                }
                for (int i = 0; i < n; i++)
                {
                    Console.WriteLine("[{2}]Radice di {0} = {1}", x, Math.Sqrt(x), i + 1);
                    x = Math.Sqrt(x);
                    if (x == 1)
                        break;
                }
            }
            catch (System.Exception e) when (x < 0 || x == double.NaN)
            {
                throw new Exception("X cannot be < 0", e);
            }
            catch (System.Exception e) when (n < 0 || n == 0)
            {
                throw new Exception("N must be a definite positive number, it cannot be 0 or less", e);
            }
            finally
            {
                Console.WriteLine("Operation Complete!");
                Console.Write("Premere un tasto per chiudere...");
                Console.Read();
            }
        }
    }
}