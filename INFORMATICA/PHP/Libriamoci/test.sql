/*
ES 1
*/

CREATE TABLE Pubblicazioni (
    Codice INTEGER,
    Titolo VARCHAR(50) NOT NULL,
    Periodicità, VARCHAR(50) NOT NULL,
    Tipo VARCHAR(50) NOT NULL,
    Prezzo DOUBLE NOT NULL,
    PRIMARY KEY (Codice)
);

CREATE TABLE Tratta (
    CodicePubblicazione INTEGER,
    CodiceArgomento INTEGER,
    PRIMARY KEY (CodicePubblicazione, CodiceArgomento),
    FOREIGN KEY (CodicePubblicazione) REFERENCES Pubblicazioni (Codice)
);

/*
ES 2
*/

SELECT CodFisc
FROM Abbonati, Abbonamenti, Pubblicazioni
WHERE Abbonamenti.CodFiscAbbonato = Abbonati.CodFisc
AND Abbonamenti.CodicePubblicazione = Pubblicazioni.Codice
AND Pubblicazioni.Titolo = "Alamut La Fortezza"
AND Abbonamenti.PeriodoValidità = "1 anno";

/*
ES 3
*/

