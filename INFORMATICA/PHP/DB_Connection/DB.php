<html>
    <head>
        <title>DB & PHP Test</title>
    </head>
    <body>
        <?php
            try 
            {
                $db = new PDO("sqlite:".__DIR__."/prova.db");
                $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);    
            } 
            catch (Exception $e) 
            {
                echo "Unable to connect";
                echo $e->getMessage();
                die;
            }

            echo "Connected!";
            $query = "SELECT id, nome, cognome FROM persone";
            $result = $db->query($query);
            echo "<table border=1>";
            echo "<tr><th>Persone</th></tr>";
            while ($row = $result->fetch(PDO::FETCH_OBJ)) 
            {
                echo "<tr><td>$row->id</td>";
                echo "<td>$row->nome</td>";
                echo "<td>$row->cognome</td></tr>";
            }
            $result->closeCursor();
            $db = NULL;
        ?>
    </body>
</html>